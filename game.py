from random import randint

# Input: Ask <name> with "Hi! What is your name?"
# Run the following up to 5 times:
#   a. Guess birthday month and year
#       "Guess <guess number> : <name> were you born in <m> / <yyyy>?"
#       Input: "yes or no?"
#   b. If computer guesses correctly because of "yes"
#       Print ("I knew it!")
#   c. If computer guesses incorrectly because of "no" with first 4 guesses
#       Print("Drat! Lemme try again!")
#   d. If computer guesses incorrectly because of "no" with guess 5
#       Print("I have other things to do. Good bye.")

name = input("Hi! What is your name?")
m = randint(1,12)
yyyy = randint(1924, 2004)
# Guess 1
print("Guess 1:", name, "were you born in", m, "/", yyyy)
answer = input("yes or no?").lower()

if answer == "yes":
    print("I knew it!")
    exit()

elif answer == "no":
    print("Drat! Lemme try again!")

# Guess 2
m = randint(1,12)
yyyy = randint(1924, 2004)
print("Guess 2:", name, "were you born in", m, "/", yyyy)
answer = input("yes or no?").lower()

if answer == "yes":
    print("I knew it!")
    exit()

elif answer == "no":
    print("Drat! Lemme try again!")

# Guess 3
m = randint(1,12)
yyyy = randint(1924, 2004)
print("Guess 3:", name, "were you born in", m, "/", yyyy)
answer = input("yes or no?").lower()

if answer == "yes":
    print("I knew it!")
    exit()

elif answer == "no":
    print("Drat! Lemme try again!")

# Guess 4
m = randint(1,12)
yyyy = randint(1924, 2004)
print("Guess 4:", name, "were you born in", m, "/", yyyy)
answer = input("yes or no?").lower()

if answer == "yes":
    print("I knew it!")
    exit()

elif answer == "no":
    print("Drat! Lemme try again!")

# Guess 5
m = randint(1,12)
yyyy = randint(1924, 2004)
print("Guess 5:", name, "were you born in", m, "/", yyyy)
answer = input("yes or no?").lower()

if answer == "yes":
    print("I knew it!")
    exit()

elif answer == "no":
    print("I have other things to do. Good bye.")
